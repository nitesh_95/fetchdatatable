package com.subk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.subk.dao.LucRepositoryInterface;
import com.subk.entity.LucModel;
import com.subk.util.RecordNotFoundException;

@Service
public class LucService {

	@Autowired
	LucRepositoryInterface repository;

	public List<LucModel> getAllEmployees(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<LucModel> pagedResult = repository.findAll(paging);

		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<LucModel>();
		}

	}

	public LucModel getEmployeeById(Integer id) throws RecordNotFoundException {
		Optional<LucModel> employee = repository.findById(id);

		if (employee.isPresent()) {
			return employee.get();
		} else {
			throw new RecordNotFoundException("No employee record exist for given id");
		}
	}

	public long getTotalUser() {
		return repository.count();
	}
	
	
	}
