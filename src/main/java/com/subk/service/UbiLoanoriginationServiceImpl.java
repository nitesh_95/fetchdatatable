package com.subk.service;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.subk.dao.UbiLoanOriginationDao;
import com.subk.dto.LoanOriginationResponse;
import com.subk.dto.LoanOriginationResponseData;

@Service
public class UbiLoanoriginationServiceImpl implements IUbiLoanOriginationService {

	@Value("${MCLRL}")
	private String mclr;
	@Value("${MCLRL_MALE}")
	private String mclrMale;
	@Value("${MCLRL_FEMALE}")
	private String mclrmalefemale;

	private String homeAddress;
	@Autowired
	UbiLoanOriginationDao ubiLoanDao;

	@Override
	public List<LoanOriginationResponse> getPendingForReviewData(String solid) {
		List<LoanOriginationResponse> value = ubiLoanDao.getPendingforReview(solid);

		for (LoanOriginationResponse resp : value) {
			resp.setBankupdateddatetime(convertDatetime(resp.getBankupdateddatetime()));
			resp.setBcupdateddatetime(convertDatetime(resp.getBcupdateddatetime()));
			resp.setFxupdateddatetime(convertDatetime(resp.getFxupdateddatetime()));
			resp.setTeupdateddatetime(convertDatetime(resp.getTeupdateddatetime()));
			resp.setUhupdateddatetime(convertDatetime(resp.getUhupdateddatetime()));



			String houseAddress = resp.getPermdistrict() + "," + resp.getPermhouseno();
			resp.setMclr(mclr);
			resp.setMclrmalefemale(mclrmalefemale);

		}

		return value;

	}

	public List<LoanOriginationResponse> getApprovedApplications(String solid) {

		List<LoanOriginationResponse> value = ubiLoanDao.getApprovedAppData(solid);
		for (LoanOriginationResponse resp1 : value) {

			resp1.setBcupdateddatetime(convertDatetime(resp1.getBcupdateddatetime()));
			resp1.setBankupdateddatetime(convertDatetime(resp1.getBankupdateddatetime()));
			resp1.setMclr(mclr);
			resp1.setMclrmalefemale(mclrmalefemale);
		}
		return value;
	}

	public List<LoanOriginationResponseData> getReturnedApplications(String solid) {
		List<LoanOriginationResponseData> value = ubiLoanDao.getReturnedAppData(solid);
		for (LoanOriginationResponseData resp1 : value) {

			resp1.setMclr(mclr);
			resp1.setMclrmalefemale(mclrmalefemale);
		}
		return value;
	}

	public List<LoanOriginationResponseData> getRejectedapplications(String solid) {
		List<LoanOriginationResponseData> value = ubiLoanDao.getRejectedApplicationsData(solid);
		for (LoanOriginationResponseData resp1 : value) {

			resp1.setMclr(mclr);
			resp1.setMclrmalefemale(mclrmalefemale);
		}
		return value;
	}

	public static String convertDatetime(String datetime) {
		if (datetime == null) {
			return null;
		}
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		String s = "";
		try {
			s = sdf2.format(sdf1.parse(datetime));
		} catch (Exception e) {
		}
		return s;
	}
}
