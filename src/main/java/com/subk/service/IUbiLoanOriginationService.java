package com.subk.service;

import java.util.List;

import com.subk.dto.LoanOriginationResponse;
import com.subk.dto.LoanOriginationResponseData;

public interface IUbiLoanOriginationService {
	
	public List<LoanOriginationResponse> getPendingForReviewData(String solid);
	
	public List<LoanOriginationResponse> getApprovedApplications(String solid);
	
	public List<LoanOriginationResponseData> getReturnedApplications(String solid);

	public List<LoanOriginationResponseData> getRejectedapplications(String solid);
}
