package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@SpringBootApplication
@EnableEurekaClient
public class UbiLoanOrginationApplication {

	public static void main(String[] args) {
		SpringApplication.run(UbiLoanOrginationApplication.class, args);
	}

}
