package com.subk.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.subk.bm.repository.IUBILoanOriginationRepository;
import com.subk.dto.LoanOriginationResponse;
import com.subk.dto.LoanOriginationResponseData;

@Repository
public class UbiLoanOriginationDao {

	@Autowired
	IUBILoanOriginationRepository ubiloanRepository;
	
	public List<LoanOriginationResponse> getPendingforReview(String solid) {
		
		return (List<LoanOriginationResponse>) ubiloanRepository.getPendingForReviewdata(solid);
	}
	
	public List<LoanOriginationResponse> getApprovedAppData(String solid) {
		return (List<LoanOriginationResponse>) ubiloanRepository.getApprovedAppdata(solid);
	}
	
	public List<LoanOriginationResponseData> getReturnedAppData(String solid){
		return (List<LoanOriginationResponseData>) ubiloanRepository.getReturnedAppdata(solid);
	}
	
	public List<LoanOriginationResponseData> getRejectedApplicationsData(String solid){
		return (List<LoanOriginationResponseData>) ubiloanRepository.getRejectedApplicationsData(solid);
	}
}
