package com.subk.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.subk.entity.LucModel;


@Repository
public interface LucRepositoryInterface extends PagingAndSortingRepository<LucModel, Integer>{
	long countById(int id);
	}


