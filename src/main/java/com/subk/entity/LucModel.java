package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "applicant_luc")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LucModel {

	
	@Id
	@Column(name = "ID")
	private Integer id;
	@Column(name = "application_id")
	private Long applicationId;
	@Column(name = "applicant_name")
	private String applicantName;
	@Column(name = "disbursement_amount")
	private String disbursementAmount;
	@Column(name = "is_fundutilized")
	private String isFundutilized;
	@Column(name = "dateofvisit")
	private String dateofvisit;

	public long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getDisbursementAmount() {
		return disbursementAmount;
	}

	public void setDisbursementAmount(String disbursementAmount) {
		this.disbursementAmount = disbursementAmount;
	}

	public String getIsFundutilized() {
		return isFundutilized;
	}

	public void setIsFundutilized(String isFundutilized) {
		this.isFundutilized = isFundutilized;
	}

	public String getDateofvisit() {
		return dateofvisit;
	}

	public void setDateofvisit(String dateofvisit) {
		this.dateofvisit = dateofvisit;
	}

	@Override
	public String toString() {
		return "LucModel [id=" + id + ", applicationId=" + applicationId + ", applicantName=" + applicantName
				+ ", disbursementAmount=" + disbursementAmount + ", isFundutilized=" + isFundutilized + ", dateofvisit="
				+ dateofvisit + "]";
	}
}
