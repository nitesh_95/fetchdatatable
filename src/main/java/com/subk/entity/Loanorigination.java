package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Loanorigination")
public class Loanorigination {
	@Id
	@Column(name = "Applicationid")
	private String applicationid;
	@Column(name="Bcupdateddatetime")
    private String bcupdateddatetime;
	@Column(name = "Appl1name")
	private String appl1name;
	@Column(name = "Mobileno")
	private String mobileno;
	@Column(name = "Permvillcity")
	private String permvillcity;
	@Column(name = "UhRecommendedLoanamount")
	private String UhRecommendedLoanamount;
	@Column(name = "Fxupdateddatetime")
	private String fxupdateddatetime;
	@Column(name = "Teupdateddatetime")
	private String teupdateddatetime;
	@Column(name = "Uhupdateddatetime")
	private String uhupdateddatetime;
	@Column(name = "Bankverificationstatus")
	private String bankverificationstatus;
	@Column(name = "Bankupdateddatetime")
	private String bankupdateddatetime;
	@Column(name="solid")
	private String solid;
	@Column(name="Loanamountsactioned")
	private String loanamountsactioned;
	@Column(name="Bankremarks")
	private String bankremarks;
	@Column(name = "noofinstalments")
	private String noofinstalments;
	@Column(name = "Instalmentamount")
	private String instalmentamount;
	@Column(name="UhRecommendedNoofmonths")
	private String uhrecommendednoofmonths;
	@Column(name="Mclr")
	private String mclr;
	@Column(name="MclrMaleFemale")
	private String mclrmalefemale;
	@Column(name="UhRecommendedInstalmentamount")
	private String uhrecommendedinstalmentamount;
	@Column(name="Dobdate")
	private String dobdate;
	@Column(name="Appl1address")
	private String appl1address;
	@Column(name = "InterestRate")
	private String interestrate;
	@Column(name = "Addressproof")
	private String addressproof;
	@Column(name = "Permhouseno")
	private String permhouseno;
	@Column(name = "permstreetno")
	private String permstreetno;
	@Column(name = "Permlandmark")
	private String permlandmark;
	@Column(name = "Permdistrict")
	private String permdistrict;
	@Column(name = "Permstate")
	private String permstate;
	@Column(name = "Permpincode")
	private String permpincode;
	@Column(name = "Commhouseno")
	
	private String commhouseno;
	@Column(name = "Commstreetno")
	private String commstreetno;
	@Column(name = "Commlandmark")
	private String commlandmark;
	@Column(name = "Commvillcity")
	private String commvillcity;
	@Column(name = "Commdistrict")
	private String commdistrict;
	@Column(name = "Commstate")
	private String commstate;
	@Column(name = "Commtelephone")
	private String commpincode;
	
	public String getCommpincode() {
		return commpincode;
	}
	public void setCommpincode(String commpincode) {
		this.commpincode = commpincode;
	}
	public String getAddressproof() {
		return addressproof;
	}
	public String getPermhouseno() {
		return permhouseno;
	}
	public void setPermhouseno(String permhouseno) {
		this.permhouseno = permhouseno;
	}
	public String getPermstreetno() {
		return permstreetno;
	}
	public void setPermstreetno(String permstreetno) {
		this.permstreetno = permstreetno;
	}
	public String getPermlandmark() {
		return permlandmark;
	}
	public void setPermlandmark(String permlandmark) {
		this.permlandmark = permlandmark;
	}
	public String getPermdistrict() {
		return permdistrict;
	}
	public void setPermdistrict(String permdistrict) {
		this.permdistrict = permdistrict;
	}
	public String getPermstate() {
		return permstate;
	}
	public void setPermstate(String permstate) {
		this.permstate = permstate;
	}
	public String getPermpincode() {
		return permpincode;
	}
	public void setPermpincode(String permpincode) {
		this.permpincode = permpincode;
	}

	public String getCommhouseno() {
		return commhouseno;
	}
	public void setCommhouseno(String commhouseno) {
		this.commhouseno = commhouseno;
	}
	public String getCommstreetno() {
		return commstreetno;
	}
	public void setCommstreetno(String commstreetno) {
		this.commstreetno = commstreetno;
	}
	public String getCommlandmark() {
		return commlandmark;
	}
	public void setCommlandmark(String commlandmark) {
		this.commlandmark = commlandmark;
	}
	public String getCommvillcity() {
		return commvillcity;
	}
	public void setCommvillcity(String commvillcity) {
		this.commvillcity = commvillcity;
	}
	public String getCommdistrict() {
		return commdistrict;
	}
	public void setCommdistrict(String commdistrict) {
		this.commdistrict = commdistrict;
	}
	public String getCommstate() {
		return commstate;
	}
	public void setCommstate(String commstate) {
		this.commstate = commstate;
	}

	public void setAddressproof(String addressproof) {
		this.addressproof = addressproof;
	}
	public String getInterestrate() {
		return interestrate;
	}
	public void setInterestrate(String interestrate) {
		this.interestrate = interestrate;
	}
	public String getApplicationid() {
		return applicationid;
	}
	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}
	public String getBcupdateddatetime() {
		return bcupdateddatetime;
	}
	public void setBcupdateddatetime(String bcupdateddatetime) {
		this.bcupdateddatetime = bcupdateddatetime;
	}
	public String getAppl1name() {
		return appl1name;
	}
	public void setAppl1name(String appl1name) {
		this.appl1name = appl1name;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getPermvillcity() {
		return permvillcity;
	}
	public void setPermvillcity(String permvillcity) {
		this.permvillcity = permvillcity;
	}
	public String getUhRecommendedLoanamount() {
		return UhRecommendedLoanamount;
	}
	public void setUhRecommendedLoanamount(String uhRecommendedLoanamount) {
		UhRecommendedLoanamount = uhRecommendedLoanamount;
	}
	public String getFxupdateddatetime() {
		return fxupdateddatetime;
	}
	public void setFxupdateddatetime(String fxupdateddatetime) {
		this.fxupdateddatetime = fxupdateddatetime;
	}
	public String getTeupdateddatetime() {
		return teupdateddatetime;
	}
	public void setTeupdateddatetime(String teupdateddatetime) {
		this.teupdateddatetime = teupdateddatetime;
	}
	public String getUhupdateddatetime() {
		return uhupdateddatetime;
	}
	public void setUhupdateddatetime(String uhupdateddatetime) {
		this.uhupdateddatetime = uhupdateddatetime;
	}
	public String getBankverificationstatus() {
		return bankverificationstatus;
	}
	public void setBankverificationstatus(String bankverificationstatus) {
		this.bankverificationstatus = bankverificationstatus;
	}
	public String getBankupdateddatetime() {
		return bankupdateddatetime;
	}
	public void setBankupdateddatetime(String bankupdateddatetime) {
		this.bankupdateddatetime = bankupdateddatetime;
	}
	public String getSolid() {
		return solid;
	}
	public void setSolid(String solid) {
		this.solid = solid;
	}
	public String getLoanamountsactioned() {
		return loanamountsactioned;
	}
	public void setLoanamountsactioned(String loanamountsactioned) {
		this.loanamountsactioned = loanamountsactioned;
	}
	public String getBankremarks() {
		return bankremarks;
	}
	public void setBankremarks(String bankremarks) {
		this.bankremarks = bankremarks;
	}
	public String getNoofinstalments() {
		return noofinstalments;
	}
	public void setNoofinstalments(String noofinstalments) {
		this.noofinstalments = noofinstalments;
	}
	public String getInstalmentamount() {
		return instalmentamount;
	}
	public void setInstalmentamount(String instalmentamount) {
		this.instalmentamount = instalmentamount;
	}
	public String getUhrecommendednoofmonths() {
		return uhrecommendednoofmonths;
	}
	public void setUhrecommendednoofmonths(String uhrecommendednoofmonths) {
		this.uhrecommendednoofmonths = uhrecommendednoofmonths;
	}
	public String getMclr() {
		return mclr;
	}
	public void setMclr(String mclr) {
		this.mclr = mclr;
	}
	public String getMclrmalefemale() {
		return mclrmalefemale;
	}
	public void setMclrmalefemale(String mclrmalefemale) {
		this.mclrmalefemale = mclrmalefemale;
	}
	public String getUhrecommendedinstalmentamount() {
		return uhrecommendedinstalmentamount;
	}
	public void setUhrecommendedinstalmentamount(String uhrecommendedinstalmentamount) {
		this.uhrecommendedinstalmentamount = uhrecommendedinstalmentamount;
	}
	public String getDobdate() {
		return dobdate;
	}
	public void setDobdate(String dobdate) {
		this.dobdate = dobdate;
	}
	public String getAppl1address() {
		return appl1address;
	}
	public void setAppl1address(String appl1address) {
		this.appl1address = appl1address;
	}
	@Override
	public String toString() {
		return "Loanorigination [applicationid=" + applicationid + ", bcupdateddatetime=" + bcupdateddatetime
				+ ", appl1name=" + appl1name + ", mobileno=" + mobileno + ", permvillcity=" + permvillcity
				+ ", UhRecommendedLoanamount=" + UhRecommendedLoanamount + ", fxupdateddatetime=" + fxupdateddatetime
				+ ", teupdateddatetime=" + teupdateddatetime + ", uhupdateddatetime=" + uhupdateddatetime
				+ ", bankverificationstatus=" + bankverificationstatus + ", bankupdateddatetime=" + bankupdateddatetime
				+ ", solid=" + solid + ", loanamountsactioned=" + loanamountsactioned + ", bankremarks=" + bankremarks
				+ ", noofinstalments=" + noofinstalments + ", instalmentamount=" + instalmentamount
				+ ", uhrecommendednoofmonths=" + uhrecommendednoofmonths + ", mclr=" + mclr + ", mclrmalefemale="
				+ mclrmalefemale + ", uhrecommendedinstalmentamount=" + uhrecommendedinstalmentamount + ", dobdate="
				+ dobdate + ", appl1address=" + appl1address + ", interestrate=" + interestrate + ", addressproof="
				+ addressproof + ", permhouseno=" + permhouseno + ", permstreetno=" + permstreetno + ", permlandmark="
				+ permlandmark + ", permdistrict=" + permdistrict + ", permstate=" + permstate + ", permpincode="
				+ permpincode + ", commhouseno=" + commhouseno + ", commstreetno=" + commstreetno + ", commlandmark="
				+ commlandmark + ", commvillcity=" + commvillcity + ", commdistrict=" + commdistrict + ", commstate="
				+ commstate + ", commpincode=" + commpincode + "]";
	}
	
}