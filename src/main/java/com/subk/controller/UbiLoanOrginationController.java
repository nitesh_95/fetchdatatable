package com.subk.controller;

import java.util.List;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.subk.core.Loginrequest;
import com.subk.dto.LoanOriginationResponse;
import com.subk.dto.LoanOriginationResponseData;
import com.subk.service.UbiLoanoriginationServiceImpl;

@CrossOrigin
@RestController

public class UbiLoanOrginationController {
	Logger log = LoggerFactory.getLogger(UbiLoanOrginationController.class);
	
	@Autowired
	UbiLoanoriginationServiceImpl loanOriginationService;

	@RequestMapping("/getPendingForReviewData")
	public List<LoanOriginationResponse> getPendingForReviewData(@RequestBody Loginrequest loginrequest)
	{
		log.info("**********Get Pending For Review called*******************");
		 List<LoanOriginationResponse> origination = loanOriginationService.getPendingForReviewData(loginrequest.getSolid());
		 log.info("**********Get Pending For Review End*******************");
		 return (List<LoanOriginationResponse>) origination;
	}
    
	@RequestMapping("/getApprovedApplications")
	public List<LoanOriginationResponse> getApprovedApplications(@RequestBody Loginrequest loginrequest)
	{
		log.info("**********Get Approved Applications called*******************");
		List<LoanOriginationResponse> origination = loanOriginationService.getApprovedApplications(loginrequest.getSolid());
		log.info("************Get Approved Applications End******************");
		return (List<LoanOriginationResponse>) origination;
	}
	
	@RequestMapping("/getReturnedApplications")
	public List<LoanOriginationResponseData> getReturnedApplications(@RequestBody Loginrequest loginrequest)
	{
		log.info("*********Get Returned Applications called*********************");
		List<LoanOriginationResponseData> origination = loanOriginationService.getReturnedApplications(loginrequest.getSolid());
		log.info("***************Get Returned Applications End*********************");
		return (List<LoanOriginationResponseData>) origination;
	}
    @RequestMapping("/getRejectedApplications")
    public List<LoanOriginationResponseData> getRejectedApplications(@RequestBody Loginrequest loginrequest)
    {
    	log.info("**************** Get Rejected Applications Called******************");
    	List<LoanOriginationResponseData> origination=loanOriginationService.getRejectedapplications(loginrequest.getSolid());
    	log.info("******Get Rejected Applications End****************");
    	return (List<LoanOriginationResponseData>) origination;
    }
}

