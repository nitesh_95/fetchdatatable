package com.subk.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.subk.entity.LucModel;
import com.subk.service.LucService;
import com.subk.util.RecordNotFoundException;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class LucControllerClass {

	@Autowired
	LucService service;

	@RequestMapping("/customer")
	public ResponseEntity<List<LucModel>> getAllEmployees(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {
		List<LucModel> list = service.getAllEmployees(pageNo, pageSize, sortBy);

		return new ResponseEntity<List<LucModel>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<LucModel> getEmployeeById(@PathVariable("id") Integer id) throws RecordNotFoundException {
		LucModel entity = service.getEmployeeById(id);

		return new ResponseEntity<LucModel>(entity, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping("/customer/count")
	private Long getNumberOfUsers() {
		return service.getTotalUser();
	}
}
