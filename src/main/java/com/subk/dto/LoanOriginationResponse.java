package com.subk.dto;

public class LoanOriginationResponse {

	private String applicationid;
	private String bcupdateddatetime;
	private String appl1name;
	private String mobileno;
	private String permvillcity;
	private String UhRecommendedLoanamount;
	private String fxupdateddatetime;
	private String teupdateddatetime;
	private String uhupdateddatetime;
	private String bankverificationstatus;
	private String bankupdateddatetime;
	private String loanamountsactioned;
	private String uhrecommendednoofmonths;
	private String noofinstalments;
	private String instalmentamount;
	private String mclr;
	private String mclrmalefemale;
	private String uhrecommendedinstalmentamount;
	private String dobdate;
	private String appl1address;
	private String addressproof;
	
	private String permhouseno;
	private String permstreetno;
	private String permlandmark;
	private String permdistrict;
	private String permstate;
	private String permpincode;
	
	private String commhouseno;
	private String commstreetno;
	private String commlandmark;
	private String commvillcity;
	private String commdistrict;
	private String commstate;
	private String commpincode;
	
	

	public LoanOriginationResponse(String applicationid,String bcupdateddatetime,String appl1name,String mobileno,
			String permvillcity,String UhRecommendedLoanamount,String fxupdateddatetime,String teupdateddatetime,
			String uhupdateddatetime,String bankverificationstatus,String bankupdateddatetime,
			String uhrecommendednoofmonths,String mclr,String mclrmalefemale,String uhrecommendedinstalmentamount,
			String dobdate,String appl1address,String addressproof,String permhouseno,String permstreetno,String permlandmark,
			String permpincode, String commhouseno,String commstreetno,String commlandmark,String commvillcity,
			String permdistrict,String permstate,String commdistrict,String commstate, String commpincode
			
			
			) {
		this.applicationid=applicationid;
		this.bcupdateddatetime=bcupdateddatetime;
		this.appl1name=appl1name;
		this.mobileno=mobileno;
		this.permvillcity=permvillcity;
		this.UhRecommendedLoanamount=UhRecommendedLoanamount;
		this.fxupdateddatetime=fxupdateddatetime;
		this.teupdateddatetime=teupdateddatetime;
		this.uhupdateddatetime=uhupdateddatetime;
		this.bankverificationstatus=bankverificationstatus;
		this.bankupdateddatetime=bankupdateddatetime;
		this.uhrecommendednoofmonths=uhrecommendednoofmonths;
		this.mclr=mclr;
		this.mclrmalefemale=mclrmalefemale;
		this.uhrecommendedinstalmentamount=uhrecommendedinstalmentamount;
		this.dobdate=dobdate;
		this.appl1address=appl1address;
		this.addressproof=addressproof;
		this.permdistrict=permdistrict;
		this.permhouseno=permhouseno;
		this.permlandmark=permlandmark;
		this.permpincode=permpincode;
		this.permstreetno=permstreetno;
		this.permvillcity=permvillcity;
		this.permstate =permstate;
		this.commdistrict = commdistrict;
		this.commhouseno = commhouseno;
		this.commlandmark = commlandmark;
		this.commpincode =  commpincode;
		this.commstate= commstate;
		this.commstreetno = commstreetno;
		this.commvillcity=commvillcity;
		
		
		
		
	}
	public LoanOriginationResponse(String applicationid,String bcupdateddatetime,String appl1name,
			String mobileno,String permvillcity,String UhRecommendedLoanamount,String loanamountsactioned,
			String bankupdateddatetime,String dobdate,String appl1address,String uhrecommendednoofmonths,
			String instalmentamount,String noofinstalments,String addressproof,String uhrecommendedinstalmentamount,String permhouseno,String permstreetno,String permlandmark,
			String permpincode, String commhouseno,String commstreetno,String commlandmark,String commvillcity,
			String permdistrict,String permstate,String commdistrict,String commstate, String commpincode) {
		this.applicationid=applicationid;
		this.bcupdateddatetime=bcupdateddatetime;
		this.appl1name=appl1name;
		this.mobileno=mobileno;
		this.permvillcity=permvillcity;
		this.UhRecommendedLoanamount=UhRecommendedLoanamount;
		this.loanamountsactioned=loanamountsactioned;
		this.bankupdateddatetime=bankupdateddatetime;
		this.dobdate=dobdate;
		this.appl1address=appl1address;
		this.uhrecommendednoofmonths=uhrecommendednoofmonths;
		this.noofinstalments=noofinstalments;
		this.instalmentamount=instalmentamount;
		this.addressproof=addressproof;
		this.uhrecommendedinstalmentamount=uhrecommendedinstalmentamount;
		this.permdistrict=permdistrict;
		this.permhouseno=permhouseno;
		this.permlandmark=permlandmark;
		this.permpincode=permpincode;
		this.permstreetno=permstreetno;
		this.permvillcity=permvillcity;
		this.permstate =permstate;
		this.commdistrict = commdistrict;
		this.commhouseno = commhouseno;
		this.commlandmark = commlandmark;
		this.commpincode =  commpincode;
		this.commstate= commstate;
		this.commstreetno = commstreetno;
		this.commvillcity=commvillcity;
		
	}
	
	public LoanOriginationResponse() {
	
	}
	
	
	


	public String getPermhouseno() {
		return permhouseno;
	}
	public void setPermhouseno(String permhouseno) {
		this.permhouseno = permhouseno;
	}
	public String getPermstreetno() {
		return permstreetno;
	}
	public void setPermstreetno(String permstreetno) {
		this.permstreetno = permstreetno;
	}
	public String getPermlandmark() {
		return permlandmark;
	}
	public void setPermlandmark(String permlandmark) {
		this.permlandmark = permlandmark;
	}
	public String getPermdistrict() {
		return permdistrict;
	}
	public void setPermdistrict(String permdistrict) {
		this.permdistrict = permdistrict;
	}
	public String getPermstate() {
		return permstate;
	}
	public void setPermstate(String permstate) {
		this.permstate = permstate;
	}
	public String getPermpincode() {
		return permpincode;
	}
	public void setPermpincode(String permpincode) {
		this.permpincode = permpincode;
	}
	public String getCommhouseno() {
		return commhouseno;
	}
	public void setCommhouseno(String commhouseno) {
		this.commhouseno = commhouseno;
	}
	public String getCommstreetno() {
		return commstreetno;
	}
	public void setCommstreetno(String commstreetno) {
		this.commstreetno = commstreetno;
	}
	public String getCommlandmark() {
		return commlandmark;
	}
	public void setCommlandmark(String commlandmark) {
		this.commlandmark = commlandmark;
	}
	public String getCommvillcity() {
		return commvillcity;
	}
	public void setCommvillcity(String commvillcity) {
		this.commvillcity = commvillcity;
	}
	public String getCommdistrict() {
		return commdistrict;
	}
	public void setCommdistrict(String commdistrict) {
		this.commdistrict = commdistrict;
	}
	public String getCommstate() {
		return commstate;
	}
	public void setCommstate(String commstate) {
		this.commstate = commstate;
	}
	public String getCommpincode() {
		return commpincode;
	}
	public void setCommpincode(String commpincode) {
		this.commpincode = commpincode;
	}
	public String getAddressproof() {
		return addressproof;
	}
	public void setAddressproof(String addressproof) {
		this.addressproof = addressproof;
	}
	public String getNoofinstalments() {
		return noofinstalments;
	}
	public void setNoofinstalments(String noofinstalments) {
		this.noofinstalments = noofinstalments;
	}
	public String getInstalmentamount() {
		return instalmentamount;
	}
	public void setInstalmentamount(String instalmentamount) {
		this.instalmentamount = instalmentamount;
	}
	public String getApplicationid() {
		return applicationid;
	}

	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}
	public String getBcupdateddatetime() {
		return bcupdateddatetime;
	}

	public void setBcupdateddatetime(String bcupdateddatetime) {
		this.bcupdateddatetime = bcupdateddatetime;
	}

	public String getAppl1name() {
		return appl1name;
	}

	public void setAppl1name(String appl1name) {
		this.appl1name = appl1name;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getPermvillcity() {
		return permvillcity;
	}

	public void setPermvillcity(String permvillcity) {
		this.permvillcity = permvillcity;
	}

	public String getUhRecommendedLoanamount() {
		return UhRecommendedLoanamount;
	}

	public void setUhRecommendedLoanamount(String uhRecommendedLoanamount) {
		UhRecommendedLoanamount = uhRecommendedLoanamount;
	}

	public String getFxupdateddatetime() {
		return fxupdateddatetime;
	}

	public void setFxupdateddatetime(String fxupdateddatetime) {
		this.fxupdateddatetime = fxupdateddatetime;
	}

	public String getTeupdateddatetime() {
		return teupdateddatetime;
	}

	public void setTeupdateddatetime(String teupdateddatetime) {
		this.teupdateddatetime = teupdateddatetime;
	}

	public String getUhupdateddatetime() {
		return uhupdateddatetime;
	}

	public void setUhupdateddatetime(String uhupdateddatetime) {
		this.uhupdateddatetime = uhupdateddatetime;
	}

	public String getBankverificationstatus() {
		return bankverificationstatus;
	}

	public void setBankverificationstatus(String bankverificationstatus) {
		this.bankverificationstatus = bankverificationstatus;
	}

	public String getBankupdateddatetime() {
		return bankupdateddatetime;
	}

	public void setBankupdateddatetime(String bankupdateddatetime) {
		this.bankupdateddatetime = bankupdateddatetime;
	}
    
	public String getLoanamountsactioned() {
		return loanamountsactioned;
	}
	public void setLoanamountsactioned(String loanamountsactioned) {
		this.loanamountsactioned = loanamountsactioned;
	}
	
	public String getUhrecommendednoofmonths() {
		return uhrecommendednoofmonths;
	}
	public void setUhrecommendednoofmonths(String uhrecommendednoofmonths) {
		this.uhrecommendednoofmonths = uhrecommendednoofmonths;
	}
	public String getMclr() {
		return mclr;
	}
	public void setMclr(String mclr) {
		this.mclr = mclr;
	}
	public String getMclrmalefemale() {
		return mclrmalefemale;
	}
	public void setMclrmalefemale(String mclrmalefemale) {
		this.mclrmalefemale = mclrmalefemale;
	}
	public String getUhrecommendedinstalmentamount() {
		return uhrecommendedinstalmentamount;
	}
	public void setUhrecommendedinstalmentamount(String uhrecommendedinstalmentamount) {
		this.uhrecommendedinstalmentamount = uhrecommendedinstalmentamount;
	}
	public String getDobdate() {
		return dobdate;
	}
	public void setDobdate(String dobdate) {
		this.dobdate = dobdate;
	}
	public String getAppl1address() {
		return appl1address;
	}
	public void setAppl1address(String appl1address) {
		this.appl1address = appl1address;
	}
	@Override
	public String toString() {
		return "LoanOriginationResponse [applicationid=" + applicationid + ", bcupdateddatetime=" + bcupdateddatetime
				+ ", appl1name=" + appl1name + ", mobileno=" + mobileno + ", permvillcity=" + permvillcity
				+ ", UhRecommendedLoanamount=" + UhRecommendedLoanamount + ", fxupdateddatetime=" + fxupdateddatetime
				+ ", teupdateddatetime=" + teupdateddatetime + ", uhupdateddatetime=" + uhupdateddatetime
				+ ", bankverificationstatus=" + bankverificationstatus + ", bankupdateddatetime=" + bankupdateddatetime
				+ ", loanamountsactioned=" + loanamountsactioned + ", uhrecommendednoofmonths="
				+ uhrecommendednoofmonths + ", noofinstalments=" + noofinstalments + ", instalmentamount="
				+ instalmentamount + ", mclr=" + mclr + ", mclrmalefemale=" + mclrmalefemale
				+ ", uhrecommendedinstalmentamount=" + uhrecommendedinstalmentamount + ", dobdate=" + dobdate
				+ ", appl1address=" + appl1address + ", addressproof=" + addressproof + ", permhouseno=" + permhouseno
				+ ", permstreetno=" + permstreetno + ", permlandmark=" + permlandmark + ", permdistrict=" + permdistrict
				+ ", permstate=" + permstate + ", permpincode=" + permpincode + ", commhouseno=" + commhouseno
				+ ", commstreetno=" + commstreetno + ", commlandmark=" + commlandmark + ", commvillcity=" + commvillcity
				+ ", commdistrict=" + commdistrict + ", commstate=" + commstate + ", commpincode=" + commpincode + "]";
	}
}