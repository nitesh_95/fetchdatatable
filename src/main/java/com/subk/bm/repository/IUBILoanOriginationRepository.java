package com.subk.bm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.dto.LoanOriginationResponse;
import com.subk.dto.LoanOriginationResponseData;
import com.subk.entity.Loanorigination;

@Repository
public interface IUBILoanOriginationRepository extends JpaRepository<Loanorigination, String>{
	
	@Query("select new com.subk.dto.LoanOriginationResponse(applicationid,bcupdateddatetime,appl1name,mobileno,permvillcity,UhRecommendedLoanamount,fxupdateddatetime,teupdateddatetime,uhupdateddatetime,bankverificationstatus,\n" + 
			"bankupdateddatetime,uhrecommendednoofmonths,mclr,mclrmalefemale,uhrecommendedinstalmentamount,dobdate,appl1address,addressproof,permhouseno,permstreetno, permlandmark,permdistrict, permstate,permpincode,commhouseno,commstreetno,commlandmark,commvillcity,commdistrict,commstate,commpincode)from Loanorigination where solid=?1 and  bankverificationstatus='pending'")
	List<LoanOriginationResponse> getPendingForReviewdata(String solid);

	@Query("select new com.subk.dto.LoanOriginationResponse(applicationid,bcupdateddatetime,appl1name,mobileno,permvillcity,UhRecommendedLoanamount,loanamountsactioned,bankupdateddatetime,dobdate,appl1address,uhrecommendednoofmonths,noofinstalments,instalmentamount,addressproof,uhrecommendedinstalmentamount,permhouseno,permstreetno, permlandmark,permdistrict, permstate,permpincode,commhouseno,commstreetno,commlandmark,commvillcity,commdistrict,commstate,commpincode)from Loanorigination where solid=?1 and  bankverificationstatus='Approved'")
	List<LoanOriginationResponse> getApprovedAppdata(String solid);
	
	@Query("select new com.subk.dto.LoanOriginationResponseData(applicationid,bcupdateddatetime,appl1name,mobileno,permvillcity,UhRecommendedLoanamount,loanamountsactioned,bankupdateddatetime,dobdate,appl1address,uhrecommendednoofmonths,addressproof,uhrecommendedinstalmentamount,mclr,mclrmalefemale,permhouseno,permstreetno, permlandmark,permdistrict, permstate,permpincode,commhouseno,commstreetno,commlandmark,commvillcity,commdistrict,commstate,commpincode)from Loanorigination where solid=?1 and  bankverificationstatus='Returned'")
	List<LoanOriginationResponseData> getReturnedAppdata(String solid);
	
	@Query("select new com.subk.dto.LoanOriginationResponseData(applicationid,bcupdateddatetime,appl1name,mobileno,permvillcity,UhRecommendedLoanamount,loanamountsactioned,bankupdateddatetime,dobdate,appl1address,uhrecommendednoofmonths,addressproof,uhrecommendedinstalmentamount,mclr,mclrmalefemale,permhouseno,permstreetno, permlandmark,permdistrict, permstate,permpincode,commhouseno,commstreetno,commlandmark,commvillcity,commdistrict,commstate,commpincode)from Loanorigination where solid=?1 and  bankverificationstatus='Rejected'")
	List<LoanOriginationResponseData> getRejectedApplicationsData(String solid);
}
